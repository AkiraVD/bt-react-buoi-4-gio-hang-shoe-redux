import React, { Component } from "react";
import { connect } from "react-redux";

class DetailsModal extends Component {
  render() {
    let shoe = this.props.detail;
    return (
      <div>
        {/* Modal Body */}
        <div
          className="modal fade"
          id="shoeDetails"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modalTitleId"
          aria-hidden="true"
        >
          <div
            className="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header"></div>
              <div className="modal-body">
                <div className="row p-4">
                  <div
                    className="col-6 image"
                    style={{
                      overflow: "hidden",
                    }}
                  >
                    <img src={shoe.image} alt="" />
                  </div>
                  <div className="col-6 d-flex flex-column justify-content-center align-items-start text-start">
                    <h2 className="mb-5 text-center w-100">{shoe.name}</h2>
                    <h4>Price: ${shoe.price}</h4>
                    <h5>Qty: {shoe.quantity}</h5>
                    <h6>{shoe.shortDescription}</h6>
                    <h6>{shoe.description}</h6>
                  </div>
                </div>
              </div>
              <div className="modal-footer"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

let mapState2Props = (state) => {
  return {
    detail: state.shoeStoreReducer.detail,
  };
};

export default connect(mapState2Props)(DetailsModal);
