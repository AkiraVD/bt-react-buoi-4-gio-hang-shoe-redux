import React, { Component } from "react";
import Cart from "./Cart";
import DetailsModal from "./DetailsModal";
import ListShoe from "./ListShoe";

export default class ShoeStore extends Component {
  render() {
    return (
      <div className="container py-5">
        <h2 className="mb-4">Shoe Store</h2>
        <Cart />
        <DetailsModal />
        <ListShoe />
      </div>
    );
  }
}
