import React, { Component } from "react";
import { connect } from "react-redux";
import ShoeItem from "./ShoeItem";

class ListShoe extends Component {
  handleRenderListShoe = () => {
    let data = this.props.data;
    return data.map((item, index) => {
      return <ShoeItem key={index} shoe={item} />;
    });
  };
  render() {
    return <div className="row">{this.handleRenderListShoe()}</div>;
  }
}

let mapState2Props = (state) => {
  return {
    data: state.shoeStoreReducer.data,
  };
};

export default connect(mapState2Props)(ListShoe);
