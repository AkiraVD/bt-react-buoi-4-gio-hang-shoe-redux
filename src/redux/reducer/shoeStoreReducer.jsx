import { dataShoe } from "../../Components/dataShoe";

let innitialState = {
  data: dataShoe,
  detail: dataShoe[0],
  cart: [],
  cartQty: 0,
  total: 0,
};

export const shoeStoreReducer = (state = innitialState, action) => {
  let calculateTotal = () => {
    let cartQty = 0;
    let totalPrice = 0;
    state.cart.forEach((item) => {
      cartQty += item.qty;
      totalPrice += item.qty * item.price;
    });
    state.cartQty = cartQty;
    state.total = totalPrice;
  };
  switch (action.type) {
    case "ADD_TO_CART": {
      let cloneCart = [...state.cart];
      let addItem = action.payload;
      let addItemIndex = cloneCart.findIndex((item) => {
        return item.id === addItem.id;
      });

      if (addItemIndex === -1) {
        cloneCart.push({ ...addItem, qty: 1 });
      } else {
        cloneCart[addItemIndex].qty++;
      }

      state.cart = [...cloneCart];
      calculateTotal();
      return { ...state };
    }
    case "CHANGE_DETAIL": {
      state.detail = action.payload;
      return { ...state };
    }
    case "ADD_NUMBER": {
      let cloneCart = [...state.cart];
      cloneCart[action.payload].qty++;
      state.cart = [...cloneCart];
      calculateTotal();
      return { ...state };
    }
    case "REDUCE_NUMBER": {
      let cloneCart = [...state.cart];
      cloneCart[action.payload].qty--;

      if (cloneCart[action.payload].qty === 0) {
        cloneCart.splice(action.payload, 1);
      }

      state.cart = [...cloneCart];
      calculateTotal();
      return { ...state };
    }
    case "CALCULATE_CHCEKOUT": {
      state.cart = [];
      state.cartQty = 0;
      state.total = 0;
      alert("Thank you for shopping");
      return { ...state };
    }
    case "REMOVE_CART_ITEM": {
      let cloneCart = [...state.cart];
      cloneCart.splice(action.payload, 1);
      state.cart = [...cloneCart];
      calculateTotal();
      return { ...state };
    }
    default:
      return state;
  }
};
